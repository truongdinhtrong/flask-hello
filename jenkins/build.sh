#!/bin/bash

set -x

docker build -t ${DOCKER_IMAGE}:${BUILD_ID} .
docker tag ${DOCKER_IMAGE}:${BUILD_ID} ${DOCKER_REPO}:${DOCKER_TAG}

