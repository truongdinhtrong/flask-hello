import docker
import re
import json, urllib.request
from urllib.error import HTTPError


def pullDockerImage(repo):
    registryDocker = 'https://index.docker.io/v1/'

    # --- read file token
    with open('.token/token_docker') as f:
        listLogin = []
        for line in f:
            line = line.strip()
            listLogin.append(line)

    username_docker = ""
    token_docker = ""

    for i in listLogin:
        if 'username' in i:
            result = re.search("^username=.*$", i)
            username_docker = result.group().split('username=')[1]
        if 'token' in i:
            result = re.search("^token=.*$", i)
            token_docker = result.group().split('token=')[1]

    client = docker.from_env()
    client.login(username=username_docker, password=token_docker, registry=registryDocker)
    container = client.containers.run(repo, detach=True)
    print('Docker id:', container.id) 

def deployDocker(url, repo):
    reponse = urllib.request.urlopen(url)
    data = json.loads(reponse.read())
    output_digest = data["results"][0]["images"][0]["digest"]

    f = open('flask_digest.txt', 'r')
    checkLastcommit = f.readlines()
    f.close()

    with open('flask_digest.txt', 'r+') as f:
        try:
            if checkLastcommit[0].strip() != output_digest.strip():
                print('deploy new image')
                f.write(output_digest)
                pullDockerImage(repo)
            else:
                print('old image, exit !!! ')
        except IndexError:
            print('file empty, deploy new image')
            f.write(output_digest)
            pullDockerImage(repo)

if __name__ == "__main__":
    deploy_flask = ['https://hub.docker.com/v2/repositories/truongdinhtrongctim/flask-hello/tags/', 'truongdinhtrongctim/flask-hello']
    # --- deploy image to production:
    deployDocker(deploy_flask[0], deploy_flask[1])



    
    
